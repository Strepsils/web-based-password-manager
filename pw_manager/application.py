from flask import Flask

from pw_manager import db, login_manager, routes


def create_app(test_config=None):
    app = Flask(__name__)

    if test_config is None:
        app.config.from_pyfile('settings.py')
        app.config.from_pyfile('local_settings.py', silent=True)
    else:
        app.config.from_pyfile(test_config)

    ctx = app.app_context()
    ctx.push()

    db.register(app)
    routes.register(app)
    login_manager.register(app)

    return app
