from flask import render_template, g, redirect, url_for
from flask_login import login_required

from pw_manager.models import AccessCredential


@login_required
def credential(ac_id):
    credential = AccessCredential.query.get_or_404(ac_id)

    if credential.owner_id != g.user.id:
        return redirect(url_for('credential_list'))

    return render_template(
        'credentials/credential.html',
        credential=credential
    )
