from flask import render_template, g, redirect, url_for
from flask_login import login_required

from pw_manager.db import db
from pw_manager.models import AccessCredential
from pw_manager.forms.record_credential import CredentialForm


@login_required
def edit(ac_id):
    credential = AccessCredential.query.get_or_404(ac_id)

    if g.user.id != credential.owner_id:
        return redirect(url_for('credential_list'))

    form = CredentialForm()

    if form.validate_on_submit():
        credential.site_name = form.site_name.data
        credential.site_url = form.site_url.data
        credential.account_name = form.account_name.data
        credential.account_password = form.account_password.data
        db.session.commit()
        return redirect(url_for('credential_list'))

    form = CredentialForm(obj=credential)
    form.account_password.data = credential.get_account_password

    return render_template(
        'credentials/credential_form.html',
        form=form,
        name='Edit',
        url_destination=url_for('credential_edit', ac_id=ac_id),
        submit_value='Save',
    )
