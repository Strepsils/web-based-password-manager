from flask import render_template, g
from flask_login import login_required


@login_required
def credential_list():
    acs_list = g.user.access_credentials.all()
    return render_template('credentials/list.html', credentials_list=acs_list)
