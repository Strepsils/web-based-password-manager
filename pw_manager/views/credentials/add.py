from flask import render_template, g, redirect, url_for
from flask_login import login_required

from pw_manager.db import db
from pw_manager.models import AccessCredential
from pw_manager.forms.record_credential import CredentialForm


@login_required
def add():
    form = CredentialForm()

    if form.validate_on_submit():
        new_credential = AccessCredential(
            site_name=form.site_name.data,
            site_url=form.site_url.data,
            account_name=form.account_name.data,
            account_password=form.account_password.data,
            owner_id=g.user.id,
        )
        db.session.add(new_credential)
        db.session.commit()
        return redirect(url_for('credential_list'))

    return render_template(
        'credentials/credential_form.html',
        form=form,
        name='Create',
        url_destination=url_for('credential_add'),
        submit_value='Add',
    )
