from flask import redirect, url_for, render_template, g
from flask_login import login_user
from pw_manager.forms.user_login import LoginForm
from pw_manager.models.user import User


def login():

    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('credential_list'))

    form = LoginForm()
    user = None
    errors = ''

    if form.validate_on_submit():

        user = User.query.filter_by(username=form.username.data).first()

        if user and user.check_password(form.password.data):
            login_user(user, remember=True)
            return redirect(url_for('credential_list'))

        errors = "Your username or password is not correct!"

    return render_template(
        'users/login.html',
        form=form,
        errors=errors
    )
