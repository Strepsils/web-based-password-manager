from flask import redirect, url_for, render_template
from flask_login import login_user

from pw_manager.forms.user_register import RegisterForm
from pw_manager.models.user import User
from pw_manager.db import db


def register():
    form = RegisterForm()
    errors = ''

    if form.validate_on_submit():
        exist_user = User.query.filter_by(username=form.username.data).first()
        if exist_user is None:
            user = User(
                username=form.username.data,
                password=form.password.data
            )
            db.session.add(user)
            db.session.commit()
            login_user(user, remember=True)
            return redirect(url_for('credential_list'))
        else:
            errors = 'Name is busy!'

    return render_template(
        'users/registration.html',
        form=form,
        errors=errors
    )
