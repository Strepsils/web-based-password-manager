from collections import namedtuple

from pw_manager import views

url = namedtuple('Route', ['path', 'endpoint', 'view', 'methods'])

routes = [
    url(
        path='/',
        endpoint='login',
        view=views.login,
        methods=['POST', 'GET']
    ),

    url(
        path='/login',
        endpoint='login',
        view=views.login,
        methods=['POST', 'GET']
    ),

    url(
        path='/logout',
        endpoint='logout',
        view=views.logout,
        methods=['GET']
    ),

    url(
        path='/register',
        endpoint='register',
        view=views.register,
        methods=['POST', 'GET']
    ),

    url(
        path='/credentials/add',
        endpoint='credential_add',
        view=views.credentials.add,
        methods=['GET', 'POST']
    ),

    url(
        path='/credentials/<ac_id>',
        endpoint='credential_view',
        view=views.credentials.credential,
        methods=['GET']
    ),

    url(
        path='/credentials/<ac_id>/edit',
        endpoint='credential_edit',
        view=views.credentials.edit,
        methods=['GET', 'POST']
    ),

    url(
        path='/credentials/<ac_id>/remove',
        endpoint='credential_remove',
        view=views.credentials.remove,
        methods=['GET']
    ),

    url(
        path='/credentials',
        endpoint='credential_list',
        view=views.credentials.credential_list,
        methods=['GET']
    ),
]


def register(app):
    for route in routes:
        app.add_url_rule(
            rule=route.path,
            endpoint=route.endpoint,
            view_func=route.view,
            methods=route.methods
        )
