import pathlib
import os


DEBUG = True
ROOT_PATH = pathlib.Path(__file__).parent
CSRF_ENABLED = True
SECRET_KEY = '9zste-seeth-mains-wsz42y'
try:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
except KeyError:
    SQLALCHEMY_DATABASE_URI = 'Not Configured'
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
