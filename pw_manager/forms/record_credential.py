from wtforms import StringField, validators
from flask_wtf import FlaskForm


class CredentialForm(FlaskForm):
    site_name = StringField('Site name')
    site_url = StringField(
        'Site url',
        [
            validators.DataRequired(),
            validators.URL(),
        ]
    )
    account_name = StringField('Account name', [validators.DataRequired()])

    account_password = StringField(
        'Account password',
        [
            validators.DataRequired()
        ]
    )
