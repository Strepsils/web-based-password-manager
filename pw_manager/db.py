from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()


def register(app):
    db.init_app(app=app)
    db.create_all(app=app)
    migrate = Migrate(app, db)
