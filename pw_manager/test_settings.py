import pathlib


TESTING = True
WTF_CSRF_ENABLED = False
SQLALCHEMY_DATABASE_URI = 'sqlite://'
DEBUG = True
ROOT_PATH = pathlib.Path(__file__).parent
SECRET_KEY = '9zsee-stest-mtest-wtesty'
SQLALCHEMY_TRACK_MODIFICATIONS = False
