from flask_login import UserMixin
from sqlalchemy.event import listens_for
from pw_manager.db import db
from pw_manager.utils import hash_encrypt_string


class User(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(100), unique=False, nullable=False)
    access_credentials = db.relationship(
        'AccessCredential',
        backref='user',
        lazy='dynamic'
    )

    def check_password(self, password):
        return hash_encrypt_string(password) == self.password

    def encrypt_password(self):
        self.password = hash_encrypt_string(self.password)

    def __repr__(self):
        return f'<User {self.username}(id: {self.id})>'


@listens_for(User, 'before_insert')
def pre_insert(mapper, connect, model: User):
    model.encrypt_password()
