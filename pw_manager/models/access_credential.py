from sqlalchemy.event import listens_for
import os

from pw_manager.db import db
from pw_manager.utils import encrypt_string, decrypt_string


class AccessCredential(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    site_name = db.Column(db.String(100), unique=False, nullable=True)
    site_url = db.Column(db.String(255), unique=False)
    account_name = db.Column(db.String(64), unique=False)
    account_password = db.Column(db.Text(), unique=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    @property
    def get_account_password(self):
        return decrypt_string(
            # ACCOUNT_SECRET_KEY - for encrypting users account passwords
            os.environ['ACCOUNT_SECRET_KEY'],
            self.account_password
        )

    def __repr__(self):
        return (
            f'<site_name: {self.site_name},'
            f'id: {self.id},'
            f'owner_id: {self.owner_id})>'
        )


@listens_for(AccessCredential.account_password, 'set', retval=True)
def password_setter(target, value, oldvalue, initiator):
    return encrypt_string(os.environ['ACCOUNT_SECRET_KEY'], value)
