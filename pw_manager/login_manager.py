
from flask import g
from flask_login import LoginManager, current_user


def register(app):
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'login'

    @login_manager.user_loader
    def load_user(user_id):
        from pw_manager.models.user import User
        try:
            return User.query.get(user_id)
        except Exception:
            return None

    @app.before_request
    def before_request():
        g.user = current_user
