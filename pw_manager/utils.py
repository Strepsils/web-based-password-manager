import hashlib
import base64

from cryptography.fernet import Fernet


def hash_encrypt_string(string: str):
    encrypted = hashlib.md5()
    encrypted.update(string.encode('utf-8'))
    return encrypted.hexdigest()


def encrypt_string(key: str, string: str):
    token = string.encode()
    f = Fernet(key)
    encrypted = f.encrypt(token)
    return encrypted.decode()


def decrypt_string(key: str, string: str):
    token = string.encode()
    f = Fernet(key)
    decrypted = f.decrypt(token)
    return decrypted.decode()
