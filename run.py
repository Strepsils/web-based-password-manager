import os

from pw_manager.application import create_app


app = create_app()
app.run(host='0.0.0.0', port=os.environ.get("PORT", 5000))
