
# Web-Based Password Manager

Password Manager as web-application, simple storage for your passwords


## Framework choice

The framework for developing this application, I decided to choose between django and flask, as I was already familiar with them. since the requirements said that 
I should reduce a boilerplate code I decided to choose the flask, since it has more flexibility and fewer ready-made solutions.


## Instruction

For start application run "python3 run.py", before start you should to set environment 
variable ACCOUNT_SECRET_KEY. for encrypting account passwords of access credentials, you can generate
key like here:

![Imgur](https://i.imgur.com/KVO68YO.png)

also you should to set URI to your database in settings.py file, variable SQLALCHEMY_DATABASE_URI or set environ variable DATABASE_URL.
And variable in environment SECRET_KEY_FOR_SHARED_LINKS for encrypting links for sharing access credentials.
