import pytest


@pytest.fixture
def crypt_key():
    return '_0MQBJNygiCsQtC_wrB5dvkdfQgatg887-8oZecBaHc='


@pytest.fixture
def string_data():
    return 'Test_String'
