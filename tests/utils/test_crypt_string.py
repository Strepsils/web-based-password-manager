from pw_manager.utils import encrypt_string, decrypt_string


def test_crypt_string(crypt_key, string_data):
    encrypted = encrypt_string(crypt_key, string_data)
    assert encrypted != string_data
    decrypted = decrypt_string(crypt_key, encrypted)
    assert decrypted == string_data
