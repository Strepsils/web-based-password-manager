from unittest import mock

from pw_manager.utils import hash_encrypt_string


def test_hash_encrypt_string():
    test_pass = 'Test_Password'
    with mock.patch('pw_manager.utils.hashlib') as mock_hashlib:
        md5_mock = mock.Mock()
        mock_hashlib.md5.return_value = md5_mock
        hash_encrypt_string(test_pass)
        mock_hashlib.md5.assert_called_once()
        md5_mock.update.assert_called_once()
