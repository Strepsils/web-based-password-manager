import pytest

from pw_manager.models import User


def test_login_failed(client):
    response = client.post('/login', data=dict(
        username='111111111',
        password='222222222',
    ))
    assert b"Your username or password is not correct!" in response.data


def test_login_success(app, client, registered_users, users_data):
    user_data, _ = users_data
    response = client.post(
        '/login',
        data=dict(
            username=user_data['username'],
            password=user_data['password'],
        ),
        follow_redirects=True,
    )
    assert response._status_code == 200
    assert b"Access Credentials" in response.data


@pytest.mark.usefixtures('logged_in_user')
def test_logout(client):
    response = client.get('/logout', follow_redirects=True)
    assert response._status_code == 200
    assert b"Login" in response.data


def test_register_success(users_data, hashed_password, client, db_session):
    user_data, _ = users_data
    response = client.post(
        '/register',
        data=dict(
            username=user_data['username'],
            password=user_data['password'],
            confirm=user_data['password'],
        ),
        follow_redirects=True,
    )
    assert response._status_code == 200
    assert b"Access Credentials" in response.data
    assert User.query.filter_by(username=user_data['username']).count() == 1
    user = User.query.filter_by(username=user_data['username']).first()
    assert user.username == user_data['username']
    assert user.password == hashed_password


@pytest.mark.parametrize(
    'login, password, confirm',
    [
        ('qwerty', '', ''),
        ('', '12345678', '12345678'),
        ('qwerty', '12345678', '87654321')
    ]
)
def test_register_validation_fail(client, login, password, confirm):
    response = client.post(
        '/register',
        data=dict(
            username=login,
            password=password,
            confirm=confirm,
        ),
    )
    assert response._status_code == 200
    assert User.query.filter_by(username=login).count() == 0


def test_register_non_uniq_name_fail(client, registered_users, users_data):
    user_data, _ = users_data
    response = client.post(
        '/register',
        data=dict(
            username=user_data['username'],
            password=user_data['password'],
            confirm=user_data['password'],
        ),
    )
    assert response._status_code == 200
    assert User.query.filter_by(username=user_data['username']).count() == 1
    assert b'Name is busy!' in response.data
