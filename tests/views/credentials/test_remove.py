import pytest

from pw_manager.models import AccessCredential


@pytest.mark.usefixtures('logged_in_user')
def test_remove_success(client, registered_users, credentials):
    user, _ = registered_users
    cred, _ = credentials
    resp = client.get(f'/credentials/{cred.id}/remove')
    assert resp._status_code == 302
    assert AccessCredential.query.get(cred.id) is None


@pytest.mark.usefixtures('logged_in_user')
def test_remove_not_valid_id_fail(client, registered_users):
    user, _ = registered_users
    resp = client.get(f'/credentials/not_valid_id/remove')
    assert resp._status_code == 404
