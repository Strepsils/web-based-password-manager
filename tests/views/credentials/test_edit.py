import pytest


@pytest.mark.usefixtures('logged_in_user')
def test_edit_success_get(client, registered_users, credentials):
    user, _ = registered_users
    cred, _ = credentials
    resp = client.get(f'/credentials/{cred.id}/edit')
    assert cred.site_url.encode() in resp.data
    assert cred.site_name.encode() in resp.data
    assert cred.account_name.encode() in resp.data
    assert cred.get_account_password.encode() in resp.data


@pytest.mark.usefixtures('logged_in_user')
def test_edit_success_post(client, registered_users, credentials):
    user, _ = registered_users
    cred, _ = credentials
    client.post(
        f'/credentials/{cred.id}/edit',
        data=dict(
            site_name='edited_name', site_url='http://edited.url',
            account_name='edited_acc_name', account_password='edited_acc_pass',
        )
    )
    assert cred.site_name == 'edited_name'
    assert cred.site_url == 'http://edited.url'
    assert cred.account_name == 'edited_acc_name'
    assert cred.get_account_password == 'edited_acc_pass'
    assert cred.owner_id == user.id


@pytest.mark.usefixtures('logged_in_user')
def test_edit_owner_and_user_mismatch_fail(
    client,
    registered_users,
    credentials
):
    _, cred_of_anouther_user = credentials
    user, _ = registered_users
    resp_get = client.get(
        f'/credentials/{cred_of_anouther_user.id}/edit',
        follow_redirects=True
    )
    assert resp_get._status_code == 200
    assert b'Access Credentials' in resp_get.data
    resp_post = client.post(
        f'/credentials/{cred_of_anouther_user.id}/edit',
        data=dict(
            site_name='edited_name', site_url='edited_url',
            account_name='edited_acc_name', account_password='edited_acc_pass',
        ),
        follow_redirects=True,
    )
    assert resp_post._status_code == 200
    assert b'Access Credentials' in resp_get.data
    assert cred_of_anouther_user.site_name != 'edited_name'
    assert cred_of_anouther_user.site_url != 'edited_url'
    assert cred_of_anouther_user.account_name != 'edited_acc_name'
    assert cred_of_anouther_user.get_account_password != 'edited_acc_pass'
    assert cred_of_anouther_user.owner_id != user.id


@pytest.mark.usefixtures('logged_in_user')
def test_edit_not_valid_id_cred(client, registered_users):
    resp = client.get(f'/credentials/not_valid/edit')
    assert resp._status_code == 404
    resp = client.post(
        f'/credentials/not_valid/edit',
        data=dict(
            site_name='edited_name', site_url='edited_url',
            account_name='edited_acc_name', account_password='edited_acc_pass',
        )
    )
    assert resp._status_code == 404
