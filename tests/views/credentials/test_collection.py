import pytest

from pw_manager.models import AccessCredential


def credential_factory(owner_id, num, db_session):
    cred = AccessCredential(
        site_name=f'Test Site{num}, owner:{owner_id}',
        site_url='test.com',
        account_name='Test_name',
        account_password='Test_password',
        owner_id=owner_id
    )
    db_session.add(cred)
    db_session.commit()
    return cred


@pytest.mark.usefixtures('logged_in_user')
def test_credentials_list(client, registered_users, db_session):
    user, _ = registered_users
    user_creds = []
    for i in range(5):
        user_creds.append(credential_factory(user.id, i, db_session))

    resp = client.get('/credentials')
    assert resp._status_code == 200

    for i in range(5):
        assert user_creds[i].site_name.encode() in resp.data
