import pytest


@pytest.mark.usefixtures('logged_in_user')
def test_credential_success(client, registered_users, credentials):
    credential, _ = credentials
    user, _ = registered_users
    resp = client.get(f'/credentials/{credential.id}')
    assert resp._status_code == 200
    assert credential.site_name.encode() in resp.data
    assert credential.site_url.encode() in resp.data
    assert credential.account_name.encode() in resp.data
    assert credential.get_account_password.encode() in resp.data
    assert user.id == credential.owner_id


@pytest.mark.usefixtures('logged_in_user')
def test_credential_not_valid_id_fail(client, registered_users):
    resp = client.get('/credentials/not_valid_id')
    assert resp._status_code == 404


@pytest.mark.usefixtures('logged_in_user')
def test_credential_user_and_owner_mismatch(
    client,
    registered_users,
    credentials
):
    _, u2_cred = credentials
    resp = client.get(f'/credentials/{u2_cred.id}')
    assert resp._status_code == 302
    resp = client.get(f'/credentials/{u2_cred.id}', follow_redirects=True)
    assert resp._status_code == 200
    assert b'Access Credentials' in resp.data
