import pytest


@pytest.mark.usefixtures('logged_in_user')
def test_add(client, registered_users):
    user, _ = registered_users
    client.post('/credentials/add', data={
        "site_name": 'test',
        "site_url": 'http://test.com',
        "account_name": 'login',
        "account_password": 'password',
    })
    credential = user.access_credentials.filter_by(site_name='test').first()
    assert credential is not None
    assert credential.site_url == 'http://test.com'
    assert credential.account_name == 'login'
    assert credential.get_account_password == 'password'
    assert credential.owner_id == user.id
