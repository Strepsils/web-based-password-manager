import pytest

from pw_manager.models import AccessCredential


@pytest.fixture
def credentials(registered_users, db_session):
    user1, user2 = registered_users
    cred1 = AccessCredential(
        site_name='Test Site',
        site_url='test.com',
        account_name='Test_name',
        account_password='Test_password',
        owner_id=user1.id
    )
    cred2 = AccessCredential(
        site_name='Test Site',
        site_url='test.com',
        account_name='Test_name',
        account_password='Test_password',
        owner_id=user2.id
    )
    db_session.add(cred1)
    db_session.add(cred2)
    db_session.commit()
    yield cred1, cred2
    db_session.delete(cred1)
    db_session.delete(cred2)
    db_session.commit()
