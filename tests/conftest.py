import pytest

from pw_manager.application import create_app
from pw_manager import models
from pw_manager.db import db
from pw_manager.utils import hash_encrypt_string


@pytest.fixture
def db_session():
    return db.session


@pytest.fixture
def users_data():
    user1_data = {
        'username': 'test_user1',
        'password': 'test_password'
    }
    user2_data = {
        'username': 'test_user2',
        'password': 'test_password',
    }
    return user1_data, user2_data


@pytest.fixture
def hashed_password(users_data):
    user1_data, _ = users_data
    return hash_encrypt_string(user1_data['password'])


@pytest.fixture
def registered_users(users_data, db_session):
    user1_data, user2_data = users_data
    user1 = models.User(**user1_data)
    user2 = models.User(**user2_data)
    db_session.add(user1)
    db_session.add(user2)
    db_session.commit()
    yield user1, user2
    db_session.delete(user1)
    db_session.delete(user2)
    db_session.commit()


@pytest.fixture
def app():
    app = create_app(test_config='test_settings.py')
    return app


@pytest.fixture
def client(app):
    with app.app_context():
        client = app.test_client()
        yield client


@pytest.fixture
def logged_in_user(client, users_data, registered_users):
    user_data, _ = users_data
    client.post(
        '/login',
        data=dict(
            username=user_data['username'],
            password=user_data['password'],
        ),
        follow_redirects=True,
    )
