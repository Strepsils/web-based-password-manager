from flask_script import Manager
from flask_migrate import MigrateCommand

from pw_manager.application import create_app
from pw_manager.db import db

app = create_app()

manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
